<?php
class API {
    public function printFullName($fullName) {
        echo "Full Name: $fullName <br>";
    }

    public function printHobbies($hobbies) {
        echo "Hobbies: <br>";
        foreach($hobbies as $hobby) {
            echo "  $hobby <br>";
        }
    }

    public function printPersonalInfo($personalInfo) {
        echo "Age : {$personalInfo->age} <br>";
        echo "Email : {$personalInfo->email} <br>";
        echo "Birthday : {$personalInfo->birthday} <br>";
    }
}

$api = new API();

// Function 1
$fullName = "Vincent V. Arellano";
$api->printFullName($fullName);

// Function 2
$hobbies = array("Basketball", "Books", "Video Games", "Guitar");
$api->printHobbies($hobbies);

// Function 3
$personalInfo = new stdClass();
$personalInfo->age = 21;
$personalInfo->email = "papaviiiiinch0821@gmail.com";
$personalInfo->birthday = "August 21, 2002";
$api->printPersonalInfo($personalInfo);